import { Injectable, EventEmitter } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';


@Injectable()
export class HttpLoadingInterceptor implements HttpInterceptor {
  start: EventEmitter<any> = new EventEmitter();
  stop: EventEmitter<any> = new EventEmitter();
  error: EventEmitter<any> = new EventEmitter();


  constructor() { }
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.start.emit();
    let targetReq = req;

    if (sessionStorage.getItem('token')) {
      targetReq = req.clone({ setHeaders: { Authorization: sessionStorage.getItem('token') } });
    }

    const handle = next.handle(targetReq)
      // .pipe(catchError((e, c) =>  throwError(e)))
      .pipe(finalize(() => { this.stop.emit(); }));

    return handle as unknown as Observable<HttpEvent<any>>;
  }
}
