import { Category } from './../models/category';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Config } from '../config';

@Injectable({
  providedIn: 'root'
})

export class ActivityService {
  constructor(private http: HttpClient) { }

  /**
       * 列出精選課程
       *
       * @param destination 目的地
       * @param departureRegion 出發地
       * @param startTime 出發時間
       * @param categoryId 分類
       * @param offset 起始索引
       * @param limit 取得筆數
       */
  listActivity(
    destination?: string,

    departureRegion?: string,


    offset: number = 0,

    limit: number = 10
  ): Observable<Category> {
    let url = '/v1/activity/categories';
    const queryList = [];

    if (offset !== null && offset !== undefined) {
      queryList.push('offset=' + encodeURIComponent(offset.toString()));
    }

    if (limit !== null && limit !== undefined) {
      queryList.push('limit=' + encodeURIComponent(limit.toString()));
    }

    return this.http.get<Category>(
      url,
      Config.defaultOptions
    ) as unknown as Observable<Category>;
  }

}
