import { Category } from './../models/category';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Config } from '../config';

@Injectable({
  providedIn: 'root'
})

export class CategoryService {
  constructor(private http: HttpClient) { }

  /**
       * 列出精選課程
       *
       * @param offset 起始索引
       * @param limit 取得筆數
       */
  listCategory(
    offset: number = 0,

    limit: number = 10
  ): Observable<Category> {
    let url = '/v1/activity/categories';
    const queryList = [];

    return this.http.get<Category>(
      url,
      Config.defaultOptions
    ) as unknown as Observable<Category>;
  }

}
