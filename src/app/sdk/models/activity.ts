export class Activity {

  /**
   *
   */
  id?: string;

  /**
   *
   */
  name?: string;

  /**
   *
   */
  description?: string;

  /**
   *
   */
  url?: string;

}
