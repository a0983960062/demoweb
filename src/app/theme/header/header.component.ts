import { LoginDialogComponent } from './../../pages/user/login-dialog/login-dialog.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AngularFireAuth } from 'angularfire2/auth';
import { CreateDialogComponent } from 'src/app/pages/masterpage/create-dialog/create-dialog.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private firebaseAuth: AngularFireAuth
  ) { }

  ngOnInit(): void {
  }

  get isLogin() {
    return sessionStorage.getItem('token');
  }

  get userName() {
    return sessionStorage.getItem('user');
  }

  openCreateDialog() {
    const dialogRef = this.dialog.open(CreateDialogComponent, {
      width: '500px',
      height: '300px',
      data: {}
    });
  }

  openLoginDialog(): void {
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '300px',
      height: '310px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(() => {
      location.reload();
    });
  }

  logout() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user');
    this.firebaseAuth.auth.signOut();
    location.reload();
  }
}
