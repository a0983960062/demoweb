import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AngularFireAuth } from 'angularfire2/auth';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit {
  email: string;
  password: string;

  constructor(
    private dialogRef: MatDialogRef<LoginDialogComponent>,
    private service: UserService,
    private firebaseAuth: AngularFireAuth
  ) {

  }

  ngOnInit(): void {
  }

  login() {
    this.service.login(this.email, this.password);
    this.dialogRef.close();
  }
}
