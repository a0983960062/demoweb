import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: Observable<firebase.User>;

  constructor(
    private firebaseAuth: AngularFireAuth
  ) {
    this.user = firebaseAuth.authState;
  }

  login(email: string, password: string) {
    sessionStorage.clear();
    this.firebaseAuth.auth.signInWithEmailAndPassword(
      email,
      password
    ).catch((error: firebase.FirebaseError) => {
      console.log(error);
    });
    this.firebaseAuth.auth.currentUser.getIdToken(true)
      .then(token => {
        sessionStorage.setItem('token', token);
        sessionStorage.setItem('user', this.firebaseAuth.auth.currentUser.displayName);
        console.log(token);
      });

  }
}
