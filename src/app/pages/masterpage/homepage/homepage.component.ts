import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  categoryList = [];

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.categoryList = this.route.snapshot.data.getCategoriesResolver;
    this.form = this.fb.group({
      destination: null,
      departure: null,
      date: null,
      category: null
    });
  }

  ngOnInit(): void {
    console.log(this.categoryList);
  }

  submit(formValue) {
    console.log(this.categoryList);
  }
}
