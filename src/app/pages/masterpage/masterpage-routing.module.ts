import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './homepage/homepage.component';
import { Routes, RouterModule } from '@angular/router';
import { ActivityDetailComponent } from './activity-detail/activity-detail.component';
import { CreateActivityComponent } from './create-activity/create-activity.component';
import { GetCategoriesResolver } from './masterpage.service';

const routes: Routes = [{
  path: '',
  pathMatch: 'full',
  redirectTo: 'find'
},
{
  path: 'find',
  component: HomepageComponent,
  resolve: {
    getCategoriesResolver: GetCategoriesResolver
  }
},
{
  path: 'create',
  component: CreateActivityComponent,
  resolve: {
    getCategoriesResolver: GetCategoriesResolver
  }
},
{
  path: 'find/:id',
  component: ActivityDetailComponent
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [GetCategoriesResolver]
})
export class MasterpageRoutingModule { }
