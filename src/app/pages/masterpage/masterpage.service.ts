import { Injectable } from '@angular/core';
import { CategoryService } from 'src/app/sdk/services';
import { Resolve } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MasterpageService {

  constructor(
    private categoryService: CategoryService
  ) { }

  getListCategory() {
    return this.categoryService.listCategory();
  }
}

@Injectable()
export class GetCategoriesResolver implements Resolve<any> {
  constructor(private service: MasterpageService) { }

  resolve() {
    return this.service.getListCategory();
  }
}
