import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-category-dialog',
  templateUrl: './category-dialog.component.html',
  styleUrls: ['./category-dialog.component.scss']
})
export class CategoryDialogComponent implements OnInit {
  selectedCategory;

  constructor(
    public dialogRef: MatDialogRef<CategoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data
  ) { }

  ngOnInit(): void {
    console.log(this.data);
  }

  confirm() {
    for (let item of this.data) {
      if (item.selected) {
        this.selectedCategory = item;
      }
    }
    this.dialogRef.close(this.selectedCategory);
    console.log(this.data);
  }
}
