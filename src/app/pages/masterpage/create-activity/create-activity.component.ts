import { CategoryService } from 'src/app/sdk/services';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { CategoryDialogComponent } from './category-dialog/category-dialog.component';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-create-activity',
  templateUrl: './create-activity.component.html',
  styleUrls: ['./create-activity.component.scss']
})
export class CreateActivityComponent implements OnInit {
  visable;
  categories;
  selectedCategory;
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private dialog: MatDialog
  ) {
    if (this.route.snapshot.paramMap.get('visable') === 'true') {
      this.visable = true;
    } else {
      this.visable = false;
    };
    this.categories = this.route.snapshot.data.getCategoriesResolver;
    this.form = this.formBuilder.group({
      type: [null, [Validators.required]],
      name: ['', [Validators.required]],
      startTime: new Date(),
      endTime: new Date(),
      count: null,
      description: null,
      questions: [],
      departure: ['', [Validators.required]],
      destination: ['', [Validators.required]],
      fee: ['', [Validators.required]]
    });

  }

  ngOnInit(): void {
  }

  openDialog() {
    for (let category of this.categories) {
      if (category.selected) {
        this.selectedCategory = category;
      }
    }

    const dialogRef = this.dialog.open(CategoryDialogComponent, {
      width: '850px',
      maxHeight: '70%',
      data: this.categories
    });

    dialogRef.afterClosed()
      .pipe(filter(x => x))
      .subscribe(result => {
        console.log(result);
        this.selectedCategory = result;
      });
  }

  submit(formVal) {

  }
}
