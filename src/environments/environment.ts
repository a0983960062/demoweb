// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBzBHI--5xQ2ZkBlZUHxQzFRmhoDXS_3ls",
    authDomain: "popmeet-staging.firebaseapp.com",
    databaseURL: "https://popmeet-staging.firebaseio.com",
    projectId: "popmeet-staging",
    storageBucket: "popmeet-staging.appspot.com",
    messagingSenderId: "138372063510",
    appId: "1:138372063510:web:6a2f501033cb492d4e3037"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
